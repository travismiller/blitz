# Blitz

Lightning Talks by Travis

## What is a Lightning Talk?

> A lightning talk is a very short presentation lasting only a few minutes, given at a conference or similar forum. Several lightning talks will usually be delivered by different speakers in a single session, sometimes called a **data blitz**. 

- https://en.wikipedia.org/wiki/Lightning_talk

## Presentation Framwork

- https://revealjs.com/
